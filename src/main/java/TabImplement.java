
public class TabImplement {
    
    public static int[] sortMethode(int[] paraTable){
        boolean sorted = false;
        while(!sorted){
            sorted = true;
            for (int i = 0; i < paraTable.length -1; i++){
                if (paraTable[i] > paraTable[i+1]){
                    var temporaire = paraTable[i];
                    paraTable[i] = paraTable[i+1];
                    paraTable[i+1] = temporaire;
                    sorted = false;
                }
            }
        }
        
        return paraTable;
    }
}
