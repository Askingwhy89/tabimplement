import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import org.junit.jupiter.api.Test;

public class TabImplementTest {
    
    public TabImplementTest() {
        
    }

    @Test
    public void testSomeMethod1() {
        int[] table = TabImplement.sortMethode(new int[]{-20,-10,0,10,20});
        assertArrayEquals(new int[]{-20,-10,0,10,20},table);
    }
    
    @Test
    public void testSomeMethod2() {
        int[] table = TabImplement.sortMethode(new int[]{-4,-2,0,2,4});
        assertArrayEquals(new int[]{-4,-2,0,2,4},table);
    }
    
}
